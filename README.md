# Bart-Homer-Classification-ANN

Classifying Homer and Bart using ANN

Compare this repository with https://github.com/avinash-218/Bart-Homer-Classification-ANN-Feature-Extraction 
which uses feature extraction and train an ANN with the extracted feature and also compare with 
https://github.com/avinash-218/Bart-Homer-Classification-CNN which uses CNN.

Also, it is to be noted that the time taken to train this model is larger than that.
 Also weights stored is around 25 GB while in that repository it is in terms offew KBs.

Also accuracy of model trained with feature extractions is better.


Summary, CNN is the best for image classification
